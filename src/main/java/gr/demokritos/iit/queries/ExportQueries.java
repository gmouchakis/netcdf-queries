/**
 * 
 */
package gr.demokritos.iit.queries;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import ucar.ma2.DataType;
import ucar.nc2.Dimension;
import ucar.nc2.NetcdfFile;
import ucar.nc2.Variable;

/**
 * @author Yiannis Mouchakis
 *
 * This class takes as input a path to a NetCDF file and for each variable that is not a dimension variable
 * prints a Hive create table query. The name of the table is the filename with all "." and "-" characters
 * replaced with "_" character. The table will get as input data a CSV file with the same structure. 
 *
 */
public class ExportQueries {

	/**
	 * @param args args[0] path to netcdf file
	 * @throws IOException 
	 */
	public static void main(String[] args) throws IOException {
		
		if (args.length != 1) {
			System.err.println("you must provide the path to the netcdf file");
			System.exit(1);
		}
		
		NetcdfFile ncfile = NetcdfFile.open(args[0]);
		
		//get dataset name
		Path ncfile_path = Paths.get(args[0]);
		
		String dataset_name = ncfile_path.getFileName().toString().replace("-", "_").replace(".", "_");

		List<Variable> var_list = ncfile.getVariables();
		List<Dimension> dim_list = ncfile.getDimensions();
		
		for (Dimension dim : dim_list) {
			String dim_name = dim.getShortName();
			if (ncfile.findVariable(dim_name) == null) {//if dimensions is not also a variable
				System.out.println("CREATE TABLE " + dataset_name + "_" + dim_name + " (row_no BIGINT) "
						+ "ROW FORMAT DELIMITED FIELDS TERMINATED BY ',';");
			}
		}

		List<Variable> measurement_variables = new ArrayList<Variable>();

		for (Variable variable : var_list) {
			if ( ! isDimensionVar(variable, dim_list)) {
				measurement_variables.add(variable);//discover measurement variables
			} else {//print dimension variable queries
				String var_name = variable.getShortName();
				System.out.println("CREATE TABLE " + dataset_name + "_" + var_name + " (row_no BIGINT, " 
						+ var_name + " " + getHiveDataType(variable.getDataType()) + ") "
						+ "ROW FORMAT DELIMITED FIELDS TERMINATED BY ',';");
			}
		}
		
		for (Variable measurement_variable : measurement_variables) {
					
			String query = "CREATE TABLE " + dataset_name + "_" + measurement_variable.getShortName() + " (row_no BIGINT, ";
			
			for (Dimension dim : measurement_variable.getDimensions()) {				
				query += dim.getShortName() + " INT, ";			
			}
			
			query += measurement_variable.getShortName() + " ";
			query += getHiveDataType(measurement_variable.getDataType());
			query += ") ROW FORMAT DELIMITED FIELDS TERMINATED BY ',';";
			
			System.out.println(query);
			
			ncfile.close();
			
		}

	}
	
	/**
     * Private helper checking if a variable is a dimension.
     * 
     * @param variable
     * @param dim_list
     */
	private static boolean isDimensionVar(Variable variable, List<Dimension> dim_list) 	{
		for( Dimension dimension : dim_list ) {
			String dimName = dimension.getShortName();
			if( (dimName != null) && dimName.equals(variable.getShortName()) )
			{ return true; }
		}
		return false;
	}

	/**
	 * 
	 * Private helper returning the Hive datatype from a NetCDF datatype.
	 * 
	 * @param dataType the netcdf datatype
	 * @return if datatype is 'LONG' then return 'BIGINT', if 'CHAR' return 'CHAR(8)', 
	 * else return datatype to upper case
	 */
	private static String getHiveDataType(DataType dataType) {
		if (dataType.equals(DataType.LONG)) {
			return "BIGINT";
		} else if (dataType.equals(DataType.CHAR)) {
			return "CHAR(8)";
		} else {
			return dataType.toString().toUpperCase();
		}
	}

}
